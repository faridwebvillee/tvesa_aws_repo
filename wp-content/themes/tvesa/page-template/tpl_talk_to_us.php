<?php 
/* Template Name: talk_to_us */
//code
 get_header();
?>
 <section id="contact_parallax" class="section-white page-title-wrapper" data-stellar-background-ratio="1" data-stellar-offset-parent="true">
			<div class="container">
				<div class="relative">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title text-center">
							<h4>Talk To Us</h4>
							<hr>
							<p>We Wanna Hear From You </p>
							<ol class="breadcrumb">
							  <li><a href="#">Home</a></li>
							  <li class="active">Talk to us</li>
							</ol>
							</div><!-- end title -->
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end relative -->
			</div><!-- end container -->
		</section><!-- end section-white -->

		<section class="section-white" style="overflow-x: hidden;max-width: 100%;" >
			<!--<div class="container">-->
				<div class="row">
					<div class="col-md-12">
						<section class="googlemap">
							<div id="map_canvas" class="wow bounceInDown animated" data-wow-duration="500ms"></div>
						</section>
					</div><!-- end col -->
				</div><!-- end row -->
<div class="container">
				<div class="row section-container">
				
					<div class="col-md-12">
						<div class="section-title text-center">
						<h4>Quick Contact</h4>
						<hr>
						<p>We Wanna Hear From You Soon!</p>
						</div><!-- end title -->
					</div><!-- end col -->
				</div><!-- end row -->

				<div class="row section-container">
					<div class="col-md-8">
					<?php echo do_shortcode('[contact-form-7 id="83" title="Contact Form"]'); ?>
					</div>
					<div class="col-md-4">
						<div class="contact-widget">
						<h4>Contact Information :</h4>
	<i class="fa fa-envelope"></i>
<p><?php echo get_option('contact_add'); ?></p>
	<small>- <?php echo get_option('contact_email'); ?></small>
	<small>- <?php echo get_option('contact_no'); ?></small>
                         
						</div><!-- end contact-widget -->

						<div class="contact-widget">
							<?php  dynamic_sidebar( 'contact_widget2'); ?>
						</div><!-- end contact-widget -->

					</div><!-- end col -->
				</div>
			</div><!-- end container -->
		</section><!-- end section-white -->

<?php get_footer(); ?>