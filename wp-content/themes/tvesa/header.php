<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package tvesa
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
 <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="keywords" content="">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<title><?php echo get_option('site_title'); ?></title>
    <link rel="shortcut icon" href="<?php echo get_option('favicon'); ?>" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-114x114.png">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fonts/font-awesome-4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/stroke.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/prettyPhoto.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<!-- hover css--->
	<link href="<?php echo get_template_directory_uri(); ?>/css/hover.css" rel="stylesheet" media="all">
	<!-- our clint css--->
	 <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsiveslides.css">
	<!-- portfolio css--->
	<link href="<?php echo get_template_directory_uri(); ?>/css/portfolio_hover/demo.css" rel="stylesheet" media="all">
	<link href="<?php echo get_template_directory_uri(); ?>/css/portfolio_hover/normalize.css" rel="stylesheet" media="all">
	<link href="<?php echo get_template_directory_uri(); ?>/css/portfolio_hover/set2.css" rel="stylesheet" media="all">	
	<!-- popup css--->	
		<link href="<?php echo get_template_directory_uri(); ?>/prettyPhoto/css/prettyPhoto.css" rel="stylesheet" media="all">	
		<!-- heading_hover css--->
	<link href="<?php echo get_template_directory_uri(); ?>/css/heading_hover/componenttt.css" rel="stylesheet" media="all">
	<link href="<?php echo get_template_directory_uri(); ?>/css/heading_hover/heading1.css" rel="stylesheet" media="all">
	<link href="<?php echo get_template_directory_uri(); ?>/css/heading_hover/demooo.css" rel="stylesheet" media="all">
	<!-- COLORS -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/custom.css">
	<!-- RS SLIDER -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/rs-plugin/css/settings.css" media="screen" />
	<link href="<?php echo get_template_directory_uri(); ?>/css/project_view.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/bop-responsive.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive-tabs.css">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="header clearfix">
			<div class="container">
				<nav class="yamm navbar navbar-default">
		          	<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			             <span class="sr-only">Toggle navigation</span>
			             <span class="icon-bar"></span>
			             <span class="icon-bar"></span>
			             <span class="icon-bar"></span>
					</button>
		            <a class="navbar-brand" href="<?php echo home_url();?>"><img src="<?php echo get_option('logo'); ?>"></a>
		          	</div>
					<div id="navbar" class="navbar-collapse collapse">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu','menu_class'=>'nav navbar-nav navbar-right' ) ); ?>
			            
					</div><!--/.nav-collapse -->
				</nav><!-- end nav -->
	      	</div><!-- end container -->
      	</header><!-- end header -->