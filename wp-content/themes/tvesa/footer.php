<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package tvesa
 */

?>

  <div class="wrapper w6">
		    <div class="container">
			    <div class="row">
				         <!--top-footer start  -->
						 
						    <div class="col-md-3 footer">
							<?php
	$my_query = new WP_Query('category_name=about_us&post_per_page=1&order=ASC'); 
    while ($my_query->have_posts()) :
    $my_query->the_post();
    $do_not_duplicate = $post->ID;
	$image= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
    ?>
							<h1><?php echo get_the_title(); ?></h1>
							  <hr>								
                                <p class="length"><?php echo get_the_excerpt(); ?></p>
													<?php 
    //$p++;
    endwhile;
    ?> 
					        </div>
				           
						   
						   <div class="col-md-3 footer">
						    <h1>SERVICES</h1> 
							   <hr>
							   <ul class="footer_ul">
						   <?php
		             $args= array('post_type'=> 'services', 'posts_per_page'=>4,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>					
								<li><span class="size"><i class="fa fa-circle"></i></span><a href="<?php echo the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
					 </ul>
					        </div>
							
							 <div class="col-md-3 footer">
							 <h1>TECHNOLOGIES</h1>
							 <hr>
							   <ul class="footer_ul">
							<?php
		             $args= array('post_type'=> 'technology', 'posts_per_page'=>4,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
					
                             <li><span class="size"><i class="fa fa-circle"></i></span><a href="<?php echo the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
					  </ul>
					        </div>
							
							
							<div class="col-md-3 footer">
							<h1>TRAINING</h1><hr>
							 <ul class="footer_ul">
							<?php
		             $args= array('post_type'=> 'training', 'posts_per_page'=>4,'order'=>'ASC');
		             query_posts($args);		
					 //$p=0;
		             if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					 ?>
					
                             <li><span class="size"><i class="fa fa-circle"></i></span><a href="<?php echo the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
 <?php 
				     //$p++;
				     endwhile;
		             endif;?>
					  </ul>
					        </div>
			  
                         <!-- top-footer end  -->					 
	            </div>
            </div>
  </div>
  
  
  <div class="wrapper w7">
		    <div class="container">
			    <div class="row">
				         <!--middle footer start  -->
				            <div class="col-md-12 footer1">  
								<?php  //dynamic_sidebar( 'social_link'); ?>
	                         <ul class="sicon">
								    <li><a href="<?php echo get_option('fb_url'); ?>" class=" hvr-bob"> <img src="<?php echo get_template_directory_uri(); ?>/image/facebook.png"></a></li>
                                    <li><a href="<?php echo get_option('twitter_url'); ?>" class=" hvr-bob"> <img src="<?php echo get_template_directory_uri(); ?>/image/twitter.png"></a></li>
							        <li><a href="<?php echo get_option('pinterest_url'); ?>" class=" hvr-bob"> <img src="<?php echo get_template_directory_uri(); ?>/image/pinterest.png"></a></li>
							        <li><a href="<?php echo get_option('instagram_url'); ?>" class=" hvr-bob"> <img src="<?php echo get_template_directory_uri(); ?>/image/instagram3.png"></a></li>
							        <li><a href="<?php echo get_option('Google_url'); ?>" class=" hvr-bob"> <img src="<?php echo get_template_directory_uri(); ?>/image/google.png"></a></li>
									 <li><a href="<?php echo get_option('youtube_url'); ?>" class=" hvr-bob"> <img src="<?php echo get_template_directory_uri(); ?>/image/youtube.png"></a></li>
								</ul>
							
					        </div>
                         <!-- middle footer end  -->					 
	            </div>
            </div>
  </div>
  
  
   <div class="wrapper w8">
		    <div class="container">
			    <div class="row">
				         <!--copyright start  -->
				            <div class="col-md-12 copyright">  
	                         <h1><?php echo get_option('copyright_text'); ?></h1>
							
					        </div>
                         <!-- copyright end  -->					 
	            </div>
            </div>
  </div>
  

	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/retina.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/wow.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/carousel.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/progress.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/parallax.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
	
	
		<!-- FILTER REV -->
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mixitup.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/wow/wow.min.js"></script>
	
	 <!-- heading_hover js--->
   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/css/heading_hover/modernizr.customm.js"></script> 
   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/css/heading_hover/demoadd.js"></script>
   
    <!-- our_clint js--->
   <script src="<?php echo get_template_directory_uri(); ?>/js/responsiveslides.min.js"></script>
   
    <!-- popup js--->
	<script src="<?php echo get_template_directory_uri(); ?>/prettyPhoto/js/jquery.prettyPhoto.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mixitup.min.js"></script>	
	 <!-- SCROLL js--->
	
	
  <script src="<?php echo get_template_directory_uri(); ?>/js/scroll.js"></script>
   <!-- Google Map -->
        <script type="text/javascript" src="<?php  echo get_template_directory_uri(); ?>/js/maps.js" type="text/javascript"></script>

  	<!-- SLIDER REV -->
	<script src="<?php echo get_template_directory_uri(); ?>/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/responsive-tabs.min.js" type="text/javascript"></script> 
    <script>
   /* ==============================================
	SLIDER -->
	=============================================== */   
	jQuery('.tp-banner').show().revolution(
	{
	dottedOverlay:"none",
	delay:16000,
	startwidth:1170,
	startheight:665,
	hideThumbs:200,     
	thumbWidth:100,
	thumbHeight:50,
	thumbAmount:5,  
	navigationType:"none",
	navigationArrows:"solo",
	navigationStyle:"preview3",  
	touchenabled:"on",
	onHoverStop:"on",
	swipe_velocity: 0.7,
	swipe_min_touches: 1,
	swipe_max_touches: 1,
	drag_block_vertical: false,          
	parallax:"mouse",
	parallaxBgFreeze:"on",
	parallaxLevels:[7,4,3,2,5,4,3,2,1,0],            
	keyboardNavigation:"off",   
	navigationHAlign:"center",
	navigationVAlign:"bottom",
	navigationHOffset:0,
	navigationVOffset:20,
	soloArrowLeftHalign:"left",
	soloArrowLeftValign:"center",
	soloArrowLeftHOffset:20,
	soloArrowLeftVOffset:0,
	soloArrowRightHalign:"right",
	soloArrowRightValign:"center",
	soloArrowRightHOffset:20,
	soloArrowRightVOffset:0,  
	shadow:0,
	fullWidth:"on",
	fullScreen:"off",
	spinner:"spinner4",  
	stopLoop:"off",
	stopAfterLoops:-1,
	stopAtSlide:-1,
	shuffle:"off",  
	autoHeight:"off",           
	forceFullWidth:"off",                         
	hideThumbsOnMobile:"off",
	hideNavDelayOnMobile:1500,            
	hideBulletsOnMobile:"off",
	hideArrowsOnMobile:"off",
	hideThumbsUnderResolution:0,
	hideSliderAtLimit:0,
	hideCaptionAtLimit:0,
	hideAllCaptionAtLilmit:0,
	startWithSlide:0
	});   
	</script>
	
	 <!-- portfolio js--->
   <script>
			// For Demo purposes only (show hover effect on mobile devices)
			[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			} );
		</script>

	 <script> 
  $(document).ready(function() {
  $("#owl-example").owlCarousel();
  $('.project-wrapper').mixItUp();
   
  $("a[rel^='prettyPhoto']").prettyPhoto();
  $("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false});
var desc="animation";
    $('.filter_ul li a').click(function(){
		var desc = $(this).html();
     
        });
});
 
  $(function() {
    $(".rslides").responsiveSlides();
  });
</script>	

<!-- skill js--->
<script>
jQuery(document).ready(function(){
	jQuery('.skillbar').each(function(){
		jQuery(this).find('.skillbar-bar').animate({
			width:jQuery(this).attr('data-percent')
		},6000);
	});
});
</script>
<script type="text/javascript">
// ==========  START GOOGLE MAP ========== //
function initialize() {
	 var gLatitude = <?php  echo get_option('longitude'); ?>;
    var gLongitude = <?php  echo get_option('latitude'); ?>;
    var gZoom = 12<?php  //echo get_option('Zoom_url'); ?>;
    var myLatLng = new google.maps.LatLng(gLatitude, gLongitude);
    var mapOptions = {
        zoom: parseInt(gZoom),
        center: myLatLng,
        disableDefaultUI: true,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'roadatlas']
        }
    };
    var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: '<?php echo get_template_directory_uri(); ?>/img/location-icon.png',
        title: '',
    });
	
}
google.maps.event.addDomListener(window, "load", initialize);

		</script>

<?php wp_footer(); ?>

</body>
</html>
