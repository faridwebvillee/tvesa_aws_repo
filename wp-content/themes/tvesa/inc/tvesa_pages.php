<?php 

if(isset($_POST['save']))
		{ 
		   $site_title = $_POST['site_title'];
		   $longitude = $_POST['longitude'];
		   $latitude = $_POST['latitude'];
		   $contact_email = $_POST['contact_email'];
		   $contact_no = $_POST['contact_no'];
		   $contact_add = $_POST['contact_add'];
		   $copyright_text = $_POST['copyright_text'];
		   $logo = $_POST['logo'];
		   $favicon = $_POST['favicon'];
		   $fb_url = $_POST['fb_url'];
			$twitter_url = $_POST['twitter_url'];
			$pinterest_url = $_POST['pinterest_url'];
			$instagram_url = $_POST['instagram_url'];
			$Google_url = $_POST['Google_url'];
			$youtube_url = $_POST['youtube_url'];
		   update_option( 'site_title', $site_title );
		   update_option( 'longitude', $longitude );
		   update_option( 'latitude', $latitude );
		   update_option( 'contact_email', $contact_email );
		   update_option( 'contact_no', $contact_no );
		   update_option( 'contact_add', $contact_add );
		   update_option( 'copyright_text', $copyright_text );
		   update_option( 'logo', $logo );
		   update_option( 'favicon', $favicon);
		   update_option( 'fb_url', $fb_url);
		   update_option( 'twitter_url', $twitter_url);
		   update_option( 'pinterest_url', $pinterest_url);
		   update_option( 'instagram_url', $instagram_url);
		   update_option( 'Google_url', $Google_url);
		   update_option( 'youtube_url', $youtube_url);
		   echo "<script>alert('Updated Successfully...');</script>";
		}

?>


<h1>Whitney Theme Options.</h1>
<form method="post" action="">

	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="banner_heading">Site Title</label>
				</th>
				<td>
					<input name="site_title" type="text"  value="<?php echo get_option('site_title'); ?>" class="regular-text">
					<p class="description">site title section</p>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="banner_heading">Longitude</label>
				</th>
				<td>
					<input name="longitude" type="text"  value="<?php echo get_option('longitude'); ?>" class="regular-text">
					<p class="description">longitude</p>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="banner_heading">latitude</label>
				</th>
				<td>
					<input name="latitude" type="text"  value="<?php echo get_option('latitude'); ?>" class="regular-text">
					<p class="description">latitude</p>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="banner_text">Contact Email</label>
				</th>
				<td>
					<input name="contact_email" type="text"  value="<?php echo get_option('contact_email'); ?>" class="regular-text">
					<p class="description">Contact Email section</p>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="banner_text">Contact No.</label>
				</th>
				<td>
					<input name="contact_no" type="text"  value="<?php echo get_option('contact_no'); ?>" class="regular-text">
					<p class="description">Contact No. section</p>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="banner_text">Contact Address</label>
				</th>
				<td>
					<input name="contact_add" type="text"  value="<?php echo get_option('contact_add'); ?>" class="regular-text">
					<p class="description">Contact Address section</p>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="banner_text">Copyright text</label>
				</th>
				<td>
					<input name="copyright_text" type="text"  value="<?php echo get_option('copyright_text'); ?>" class="regular-text">
					<p class="description">Copyright text section</p>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="banner_image">Logo</label>
				</th>
				<td>
					<input name="logo" type="text" id="banner_image"   class="regular-text" value="<?php echo get_option('logo'); ?>">
					<a class="image-uploader btn submit-btns-full" style="cursor:pointer;">Upload Image</a>
					<p class="description">Logo image section</p>
					<?php
					if(get_option('logo')) {
					?>
					<img src="<?php echo get_option('logo'); ?>">
					<?php } ?>
				</td>
			</tr>
			
			<tr>
				<th scope="row">
					<label for="banner_image">Favicon</label>
				</th>
				<td>
					<input name="favicon" type="text" id="favicon_image"   class="regular-text" value="<?php echo get_option('favicon'); ?>">
					<a class="image-uploader btn submit-btns-full" style="cursor:pointer;">Upload Image</a>
					<p class="description">Favicon image section (favicon image must be 16X16)</p>
					<?php
					if(get_option('favicon')) {
					?>
					<img src="<?php echo get_option('favicon'); ?>">
					<?php } ?>
				</td>
			</tr>
	
	     <tr>
				<th scope="row">
					<label for="banner_text">Facebook Url</label>
				</th>
				<td>
					<input name="fb_url" type="text"  value="<?php echo get_option('fb_url'); ?>" class="regular-text">
					<p class="description">facebook url section</p>
				</td>
			</tr>		
			
			<tr>
				<th scope="row">
					<label for="banner_text">Twitter Url</label>
				</th>
				<td>
					<input name="twitter_url" type="text"  value="<?php echo get_option('twitter_url'); ?>" class="regular-text">
					<p class="description">twitter url section</p>
				</td>
			</tr>	
			
			<tr>
				<th scope="row">
					<label for="banner_text">Pinterest Url</label>
				</th>
				<td>
					<input name="pinterest_url" type="text"  value="<?php echo get_option('pinterest_url'); ?>" class="regular-text">
					<p class="description">Pinterest url section</p>
				</td>
			</tr>	
  			
  			<tr>
				<th scope="row">
					<label for="banner_text">Instagram Url</label>
				</th>
				<td>
					<input name="instagram_url" type="text"  value="<?php echo get_option('instagram_url'); ?>" class="regular-text">
					<p class="description">instagram url section</p>
				</td>
			</tr>	
			
			
  			<tr>
				<th scope="row">
					<label for="banner_text">Google Url</label>
				</th>
				<td>
					<input name="Google_url" type="text"  value="<?php echo get_option('Google_url'); ?>" class="regular-text">
					<p class="description">Google url section</p>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="banner_text">Youtube Url</label>
				</th>
				<td>
					<input name="youtube_url" type="text"  value="<?php echo get_option('youtube_url'); ?>" class="regular-text">
					<p class="description">Youtube url section</p>
				</td>
			</tr>
			
		</tbody>
	</table>


	<p class="submit"><input type="submit" name="save" id="submit" class="btn submit-btns-full" value="Save Changes"></p>
</form>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script>
 $(document).ready(function(){
 $('.image-uploader').click(function() {
	   upload_image_button =true;
       formfieldID=jQuery(this).prev().attr('id');
       //formfieldimg=jQuery(this).next().attr('id');
       tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
       if(upload_image_button==true){
               var oldFunc = window.send_to_editor;
               window.send_to_editor = function(html) {
              imgurl = jQuery('img', html).attr('src');
               jQuery('#'+formfieldID).val(imgurl);
             // jQuery('#'+formfieldimg).attr('src',imgurl); 
                tb_remove();
               window.send_to_editor = oldFunc;
                               }
       }
       upload_image_button=false; 
              
   });
   
});
</script>