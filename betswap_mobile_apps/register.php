<?php
header("Access-Control-Allow-Origin: *");
include("connection.php");

$uesername = isset($_REQUEST['username']) ? $_REQUEST['username'] : "";
$password = isset($_REQUEST['pass']) ? $_REQUEST['pass'] : "";
$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : "";
$fname = isset($_REQUEST['fname']) ? $_REQUEST['fname'] : "";
$lname = isset($_REQUEST['lname']) ? $_REQUEST['lname'] : "";
$contact = isset($_REQUEST['contact']) ? $_REQUEST['contact'] : "";
$sports = isset($_REQUEST['sports']) ? $_REQUEST['sports'] : "";
$bookmakers = isset($_REQUEST['bookmakers']) ? $_REQUEST['bookmakers'] : "";
$aws = isset($_REQUEST['aws']) ? $_REQUEST['aws'] : "";
$country = isset($_REQUEST['country']) ? $_REQUEST['country'] : "";
$state = isset($_REQUEST['state']) ? $_REQUEST['state'] : "";
$today = date("Y-m-d H:i:s"); 
$password_hash = password_hash ( $password , PASSWORD_BCRYPT );

$sql_insert = "INSERT INTO `members` (`email`,`username`,`passhash`,`firstname`,`lastname`,`phone`,`dob`,`country`,`state`,`avgspend`,`facebook_id`,`facebook_token`,`braintree_token`,`profile_url`,`bsb`,`account`,`added`,`verified`,`verify_code`) VALUES ('$email','$uesername','$password_hash','$fname','$lname','$contact','$contact','$country','$state','$aws','','','','','','','$today','0','')";
$res = mysqli_query($con,$sql_insert);
$insert_id = mysqli_insert_id($con);
if($insert_id > 0){
   $temp_sports = explode(",",$sports); 
   for($i=0;$i<count($temp_sports);$i++){
   	$sport_id = $temp_sports[$i];
   	$sql_spi = "insert into `membersports` (`member_id`,`sport_id`) values ('$insert_id','$sport_id')";
   	$r = mysqli_query($con,$sql_spi);
	}
	$temp_bookmakers = explode(",",$bookmakers); 
   for($i=0;$i<count($temp_bookmakers);$i++){
   	$bookmakers_id = $temp_bookmakers[$i];
   	$sql_bmi = "insert into `memberbookmakers` (`member_id`,`bookmaker_id`) values ('$insert_id','$bookmakers_id')";
   	$r1 = mysqli_query($con,$sql_bmi);
	}
	header('Content-Type: application/json');
	echo json_encode(array("status"=>1));
}
else{
	header('Content-Type: application/json');
   echo json_encode(array("status"=>0));
}
?>