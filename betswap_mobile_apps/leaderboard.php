<?php 

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include("connection.php");
$arr = array();
$time_frame = isset($_POST['time_frame']) ? $_POST['time_frame'] : "";
$sport_id = isset($_POST['sport_id']) ? $_POST['sport_id'] : "";
$league = isset($_POST['league']) ? $_POST['league'] : "";
$page_no = isset($_POST['page_no']) ? $_POST['page_no'] : 1;
$page = ($page_no - 1) * 20;
if($time_frame == 'month') {
			$date = date("Y-m-d H:i:s",mktime(0,0,0,date("n")-1,date("j"),date("Y")));
		} else if($time_frame == 'week') {
			$date = date("Y-m-d H:i:s",mktime(0,0,0,date("n"),date("j")-7,date("Y")));
		} else if($time_frame == '90days') {
			$date = date("Y-m-d H:i:s",mktime(0,0,0,date("n"),date("j")-90,date("Y")));
		} else if($time_frame == 'year') {
			$date = date("Y-m-d H:i:s",mktime(0,0,0,date("n"),date("j"),date("Y")-1));
		} else {
			$date = date("Y-m-d H:i:s",mktime(0,0,0,0,0,0));
		}
		
		if($sport_id == "" && $league == ""){
			 $sql = "select members.*,sum(tipscore.profit) as profit from members,tipscore where members.id = tipscore.member_id AND tipscore.is_void = '0' AND tipscore.added > '$date' GROUP BY tipscore.member_id ORDER BY profit DESC LIMIT $page,20";
		}
		elseif($sport_id != "" && $league == ""){
			$sql = "select members.*,sum(tipscore.profit) as profit from members,tipscore where members.id = tipscore.member_id AND tipscore.is_void = '0' AND tipscore.added > '$date' and tipscore.tip_id in (select tip_id from tipitems where cat_id = $sport_id) GROUP BY tipscore.member_id ORDER BY profit DESC LIMIT $page,20";
		}
		elseif($sport_id != "" && $league != ""){
			$sql = "select members.*,sum(tipscore.profit) as profit from members,tipscore where members.id = tipscore.member_id AND tipscore.is_void = '0' AND tipscore.added > '$date' and tipscore.tip_id in (select tip_id from tipitems where event_id = $league) GROUP BY tipscore.member_id ORDER BY profit DESC LIMIT $page,20";
		}
		
		
		$res = mysqli_query($con,$sql);
		$i = 0;
		while($post_main = mysqli_fetch_assoc($res)){
			$user_id = $post_main['id'];
			$availale_tips = " select count(id) as available_count from membertips where member_id = $user_id and expires > NOW()";
			$res_available = mysqli_query($con,$availale_tips);
			$avaliable_tips_count = mysqli_fetch_assoc($res_available);
			$roi = 0;
			$sql_roi = "select * FROM tipscore where member_id = $user_id AND is_void = '0' ORDER BY id DESC";
			$res_roi = mysqli_query($con,$sql_roi);
			$counter_roi = mysqli_num_rows($res_roi);
			if($counter_roi <= 0){
				$roi = 0;
			}
			else{
				$post = mysqli_fetch_assoc($res_roi);
				$running_profit = $post['running_profit'];
				$running_stake = $post['running_stake'];
				$roi = ($running_profit/$running_stake)*100;
			}
			$roi = number_format($roi,2).'%';
			$count_win = 0;
			$count_loss = 0;
			$sqlcount_win = "select count(*) as total_count from tipscore where member_id = $user_id AND profit > 0 AND is_void = '0'";
			$res_sqlcount_win = mysqli_query($con,$sqlcount_win);
			$postcount_win = mysqli_fetch_assoc($res_sqlcount_win);
			$count_win = $postcount_win['total_count'];
			
			$sqlcount_loss = "select count(*) as total_count from tipscore where member_id = $user_id AND profit <= 0 AND is_void = '0'";
			$res_sqlcount_loss = mysqli_query($con,$sqlcount_loss);
			$postcount_loss = mysqli_fetch_assoc($res_sqlcount_loss);
			$count_loss = $postcount_loss['total_count'];
			$data[$i]['total_win'] = $count_win;
			$data[$i]['total_loss'] = $count_loss;
			
			
			$arr[$i] = $post_main;
			$arr[$i]['available_tips'] = $avaliable_tips_count['available_count'];
			$arr[$i]['roi'] = $roi;
			$arr[$i]['total_wins'] = $count_win;
			$arr[$i]['total_loss'] = $count_loss;
			$i++;
		}
		echo json_encode(array('status'=>1,'data'=>$arr));
		function is_past($event_date){

$utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",time()),
			    new DateTimeZone('UTC')
		);

		$mytime = clone $utc_date;
	 $mytime->setTimeZone(new DateTimeZone(date_default_timezone_get()));
	 $utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",strtotime($event_date)),
			    new DateTimeZone('UTC')
			);

			$acst_date = clone $utc_date;
			$acst_date->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			$past = ($acst_date<$mytime)?1:0;
          
		return $past;
}
?>