<?php
header("Access-Control-Allow-Origin: *");
include("connection.php");

$sql = "SELECT * FROM categories WHERE parent_id = 0 ORDER BY name";
$res = mysqli_query($con,$sql);
$result = array();
$i = 0;
while($post=mysqli_fetch_assoc($res)){
    $id = $post['id'];
	$result[$i]['id'] = $post['id'];
   $result[$i]['name'] = $post['name'];   
   $sql3 = "select count(id) as counter  from markets where cat_id in (select id from marketcategories where event_id in (select id from events where date > NOW() and category_id in (select id from categories WHERE parent_id = $id ORDER BY name)))";
	$res3 = mysqli_query($con,$sql3);
	$counter = 0;
	while($p = mysqli_fetch_assoc($res3)){
		$counter = $p['counter'];
	}
	$result[$i]['market_count'] = $counter;
   $i++;
}

header('Content-Type: application/json');
if(count($result)<0)
   echo json_encode(array("status"=>0,"data"=>array()));
else 
   echo  json_encode(array("status"=>1,"data"=>$result));   

?>