<?php

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include("connection.php");
    
    try{

$soprt_id = isset($_POST['sport_id']) ? $_POST['sport_id'] : 2948;
$league_id = isset($_POST['league_id']) ? $_POST['league_id'] : 3952;
$page = isset($_POST['page']) ? $_POST['page'] : 0;
$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : 0;
$results = array();

	$sql_get_tid = "SELECT distinct tip_id FROM tipitems where cat_id = $league_id"; 
	$res_get_tid = mysqli_query($con,$sql_get_tid);
   $tid_arr = array();
   $tid_str = "0";
   while($ip = mysqli_fetch_assoc($res_get_tid)){
	    $tid_arr[] = $ip['tip_id'];
	}
	if(count($tid_arr) > 0){
	    $tid_str = implode(",",$tid_arr);
	}
	$sql_count = "SELECT * FROM membertips where id in ($tid_str) and expires > NOW() order by expires ASC";
	$r_count = mysqli_query($con,$sql_count);
	$page = $page*30;
	$sql = "SELECT * FROM membertips where id in ($tid_str) and expires > NOW() order by expires ASC limit $page , 30";	
     $res = mysqli_query($con,$sql);
     $j = 0;
     while($post = mysqli_fetch_assoc($res)){
     	   $id = $post['id'];
     	   $results[$j]['id'] = $id;
     	   $results[$j]['bettype'] = $post['bettype'];
     	   $results[$j]['multistake'] = $post['multistake'];
     	   $results[$j]['price'] = $post['price'];  
		   $purchased_status = get_purchase_status($con,$id,$user_id);
		   $results[$j]['purchased_status'] = $purchased_status;
     	   $sql_ti = "select * from tipitems where tip_id = '$id'";
     	   $res_ti = mysqli_query($con,$sql_ti);
           $like_count = get_like_dislike($id,$con,1);
		   $dislike_count = get_like_dislike($id,$con,0);
		   $comment_count = get_comment_count($id,$con);
           $results[$j]['like_count'] = $like_count; 
		   $results[$j]['dislike_count'] = $dislike_count; 		   
		   $results[$j]['comment_count'] = $comment_count; 
			
		   $uid = $post['member_id'];
           
          $sql_get_user_detail = "SELECT * FROM members where id = $uid";
          $res_user = mysqli_query($con,$sql_get_user_detail);
		  $user_arr=[];
		  $user_arr['member_id'] = $post['member_id']; 
         while($r = mysqli_fetch_assoc($res_user)){
		 
 		     $user_arr['username'] = $r['username'];
            
             $user_arr['member_email'] = $r['email'];
             $user_arr['member_image'] = $r['profile_url'];
             $user_arr['firstname'] = $r['firstname'];
             $user_arr['lastname'] = $r['lastname'];
			 $wins = get_user_betting_result($con,$uid,"win");
			 $loss = get_user_betting_result($con,$uid,"loss");
			 $profit = get_user_profit($con,$uid);
			 $user_arr['wins'] = $wins;
			 $user_arr['loss'] = $loss;
			 $user_arr['profit'] = $profit;
         }
         $results[$j]['user_detail'][0]=$user_arr;
         
     	   $i = 0;
     	   while($p = mysqli_fetch_assoc($res_ti)){
     	   	$market_id = $p['market_id']; 
			$sql_market_details = "select * from markets where id = $market_id";
			$res_market_details = mysqli_query($con,$sql_market_details);
			$po_market_details = mysqli_fetch_assoc($res_market_details);
			$market_cat_id = $po_market_details['cat_id'];
			$sql_market_cat_name = "select * from marketcategories where id = $market_cat_id";
			$res_market_cat_name = mysqli_query($con,$sql_market_cat_name);
			$po_market_cat_name = mysqli_fetch_assoc($res_market_cat_name);
     	   	$results[$j]['tips_item'][$i]['market_id'] = $p['market_id']; 
			$results[$j]['tips_item'][$i]['market_name'] = $po_market_details['name'];
			$results[$j]['tips_item'][$i]['market_cat_name'] = $po_market_cat_name['name'];
     	   	$results[$j]['tips_item'][$i]['event_id'] = $p['event_id']; 
     	   	$results[$j]['tips_item'][$i]['cat_id'] = $p['cat_id'];
     	   	$results[$j]['tips_item'][$i]['odds'] = $p['odds']; 
     	   	$results[$j]['tips_item'][$i]['stake'] = $p['stake'];
			$is_voide = $p['is_void'];	
			$is_marked = $p['is_marked'];	
			$check_required = $p['check_required'];	
			$result_event = $p['result'];	
			
			$cat_id = $p['cat_id'];
			$cat_name = get_cat_name_by_id($cat_id,$con);
            $results[$j]['tips_item'][$i]['cat_name'] = $cat_name;   
               $eid = $p['event_id'];
               
                $sql_get_user_detail = "SELECT * FROM events where id = $eid";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  $results[$j]['tips_item'][$i]['event_name'] = $r['name'];
                  $results[$j]['tips_item'][$i]['event_date'] = $r['date'];
				  
                  $results[$j]['tips_item'][$i]['event_venue'] = $r['venue'];
				  $past = is_past($r['date']);			  
               }
			
			$flag = 0;
            if($past == 1){
			    if($check_required == 1){
				    $flag = 'Verification Needed';
				} 
				elseif($is_marked == 0){
				    $flag = 'Result: Pending';
				}
				elseif($result_event == 1){
					$flag = "Result: WIN";
				}
				elseif($is_voide == 0){
				    $flag = "Result: LOSS";
				}
				else{
				    $flag = "Result: VOID"; 
				}
			}           
            $results[$j]['tips_item'][$i]['result'] = $flag;   
     	   	$i++; 
     	   }
     	   $j++; 
     } 

if(count($results) <= 0){
	echo json_encode(array('status'=>0,'data'=>array()));
}
else{
   echo json_encode(array('status'=>1,'data'=>$results));
}
        
    }catch(Excepton $e){
        echo json_encode(array('status'=>1,'data'=>$results));
    }
	
	
function is_past($event_date){

$utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",time()),
			    new DateTimeZone('UTC')
		);

		$mytime = clone $utc_date;
	 $mytime->setTimeZone(new DateTimeZone(date_default_timezone_get()));
	 $utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",strtotime($event_date)),
			    new DateTimeZone('UTC')
			);

			$acst_date = clone $utc_date;
			$acst_date->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			$past = ($acst_date<$mytime)?1:0;
          
		return $past;
}	

function get_cat_name_by_id($cat_id,$con){

                   $sql_get_user_detail = "select name from `categories` where id = $cat_id ";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['name'];		  
               }
            return "-";
}

function get_like_dislike($id,$con,$status){
     $sql_get_user_detail = "select count(id) as total from `tiplikes` where tip_id = $id and liketype = '$status'";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['total'];		  
               }
            return "0";
}

function get_comment_count($id,$con){
     $sql_get_user_detail = "select 	count(id) as total from `tipcomments` where tip_id = $id";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['total'];		  
               }
            return "0";

}

function get_user_betting_result($con,$uid,$status){

		$count = 0;
		if($status == "win"){
		    $sql = "select count(*) as total_count from tipscore where member_id = $uid AND profit > 0 AND is_void = '0'";
			$res_sql = mysqli_query($con,$sql);
			$post = mysqli_fetch_assoc($res_sql);
			$count = $post['total_count'];
		}
		if($status == "loss"){
		    $sql = "select count(*) as total_count from tipscore where member_id = $uid AND profit <= 0 AND is_void = '0'";
			$res_sql = mysqli_query($con,$sql);
			$post = mysqli_fetch_assoc($res_sql);
			$count = $post['total_count'];
		}
		
	
	return $count;
}

function get_user_profit($con,$uid){
    $sql_get_user_detail = "select running_profit from tipscore where member_id = $uid order by id DESC limit 1";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['running_profit'];		  
               }
       return "0";
}

function get_purchase_status($con,$id,$user_id){

	$sql_self = "select * from membertips where id = $id and member_id = $user_id";
	$res_self = mysqli_query($con,$sql_self);
	$count_self = mysqli_num_rows($res_self);
	if($count_self > 0){
		return "yes";
	}
	else{
		$sql = "select * from tippurchases where member_id = $user_id and tip_id = $id";
		$res = mysqli_query($con,$sql);
		$count = mysqli_num_rows($res);
		if($count > 0){
			return "yes";
		}
		else{
			return "no";
		}
	}
}
?>