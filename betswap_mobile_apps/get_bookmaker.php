<?php
header("Access-Control-Allow-Origin: *");
include("connection.php");
$main = array();
$sql_bookmaker = "SELECT * FROM bookmakers";
$res_bookmaker = mysqli_query($con,$sql_bookmaker);
$i = 0;
while($res = mysqli_fetch_assoc($res_bookmaker)){
    $main[$i]['id'] = $res['id'];
    $main[$i]['name'] = $res['name'];
    $main[$i]['url'] = $res['url'];
    $main[$i]['credit'] = $res['credit'];
	$main[$i]['logo_url'] = $res['logo_url'];
	$main[$i]['highlight'] = $res['highlight'];
    $main[$i]['info'] = $res['info']; 	
    $main[$i]['review'] = $res['review'];	
    $i++; 
}
echo json_encode(array("status"=>1,"data"=>$main));
?>