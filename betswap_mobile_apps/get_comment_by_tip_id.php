<?php 
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include("connection.php");

$tip_id = isset($_POST['tip_id']) ? $_POST['tip_id'] : 976;
$result = array();
$sql = "select * from tipcomments where tip_id = $tip_id";
$res = mysqli_query($con,$sql);
$i = 0;
while($post = mysqli_fetch_assoc($res)){

	$user_id = $post['member_id'];
	$comment_text = $post['comment'];
	$sql_usr = "select * from members where id = $user_id";
	$res_usr = mysqli_query($con,$sql_usr);
	$post_usr = mysqli_fetch_assoc($res_usr);
	$user_name = $post_usr['firstname']." ".$post_usr['lastname'];
	$profile_url = $post_usr['profile_url'];
	$result[$i]['comment_text'] = $comment_text;
	$result[$i]['user_id'] = $user_id;
	$result[$i]['user_name'] = $user_name;
	$result[$i]['profile_url'] = $profile_url;
	$i++;
	
}
echo json_encode(array('status'=>1,'data'=>$result));
?>