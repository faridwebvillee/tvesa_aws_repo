<?php 

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include("connection.php");

$user_id = isset($_POST['uid']) ? $_POST['uid'] : 343;
$data = array();
$sql_following = "select * from followers where follower_id = $user_id";
$res = mysqli_query($con,$sql_following);
$i=0;
while($post = mysqli_fetch_assoc($res)){

	$following_id = $post['member_id']; 
	$sql = "select * from members where id = '$following_id'";
	$re = mysqli_query($con,$sql);
    while($p = mysqli_fetch_assoc($re)){
		$data[$i]['following_id'] = $p['id']; 
		$data[$i]['name'] = $p['firstname']." ".$p['lastname']; 
		$data[$i]['profile_url'] = $p['profile_url']; 
		$id = $p['id'];
		$roi = 0;
		$sql_roi = "select * FROM tipscore where member_id = $id AND is_void = '0' ORDER BY id DESC";
		$res_roi = mysqli_query($con,$sql_roi);
		$counter_roi = mysqli_num_rows($res_roi);
		if($counter_roi <= 0){
			$roi = 0;
		}
		else{
			$post_roi = mysqli_fetch_assoc($res_roi);
			$running_profit = $post_roi['running_profit'];
			$running_stake = $post_roi['running_stake'];
			$roi = ($running_profit/$running_stake)*100;
		}	
		$data[$i]['roi'] = number_format($roi,2).'%';
		
		$count_win = 0;
		$count_loss = 0;
		$sqlcount_win = "select count(*) as total_count from tipscore where member_id = $user_id AND profit > 0 AND is_void = '0'";
		$res_sqlcount_win = mysqli_query($con,$sqlcount_win);
		$postcount_win = mysqli_fetch_assoc($res_sqlcount_win);
		$count_win = $postcount_win['total_count'];
		
		$sqlcount_loss = "select count(*) as total_count from tipscore where member_id = $user_id AND profit <= 0 AND is_void = '0'";
		$res_sqlcount_loss = mysqli_query($con,$sqlcount_loss);
		$postcount_loss = mysqli_fetch_assoc($res_sqlcount_loss);
		$count_loss = $postcount_loss['total_count'];
		$data[$i]['total_win'] = $count_win;
		$data[$i]['total_loss'] = $count_loss;
	}
	$i++;
}
echo json_encode(array('status'=>1,'data'=>$data));



	function is_past($event_date){

$utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",time()),
			    new DateTimeZone('UTC')
		);

		$mytime = clone $utc_date;
	 $mytime->setTimeZone(new DateTimeZone(date_default_timezone_get()));
	 $utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",strtotime($event_date)),
			    new DateTimeZone('UTC')
			);

			$acst_date = clone $utc_date;
			$acst_date->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			$past = ($acst_date<$mytime)?1:0;
          
		return $past;
}
?>