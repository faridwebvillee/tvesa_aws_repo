<?php 

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include("connection.php");

$user_id = isset($_POST['uid']) ? $_POST['uid'] : 343;

$results = array();
$sql_user = "select * from members where id = $user_id";
$res_user = mysqli_query($con,$sql_user);

while($post = mysqli_fetch_assoc($res_user)){
	$results['basic_info'] = $post;
}



$sql_achivements = "SELECT A.name, A.icon_url FROM achievements A JOIN memberachievements MA ON A.id = MA.achievement_id WHERE MA.member_id = $user_id";
$res_achivements = mysqli_query($con,$sql_achivements);
while($pa = mysqli_fetch_assoc($res_achivements)){
	$results['achievements'][] = $pa;
}

$sql_card_info = "select * from creditcards where member_id = $user_id";
$res_cardinfo = mysqli_query($con,$sql_card_info);

while($post = mysqli_fetch_assoc($res_cardinfo)){
	$results['card_details'][] = $post;
}


$sql_follower = "select count(id) as total from followers where member_id = $user_id";
$res_follower = mysqli_query($con,$sql_follower);
$post_follower = mysqli_fetch_assoc($res_follower);
$total_follower = $post_follower['total'];

$sql_following = "select count(id) as total from followers where follower_id = $user_id";
$res_following = mysqli_query($con,$sql_following);
$post_following = mysqli_fetch_assoc($res_following);
$total_following = $post_following['total'];

$results['follow_details']['follower'] = $total_follower;
$results['follow_details']['following'] = $total_following;

$wins = get_user_betting_result($con,$user_id,"win");
$loss = get_user_betting_result($con,$user_id,"loss");
$profit = get_user_profit($con,$user_id);
$roi = roipercent($user_id,$con);
$lastfive = "";
$sql_last_five = "select profit FROM tipscore WHERE member_id = $user_id ORDER BY added DESC LIMIT 5";
$res_last_five = mysqli_query($con,$sql_last_five);
while($pl = mysqli_fetch_assoc($res_last_five)){
	$profites = $pl['profit'];
	if($profites > 0){
		$lastfive = $lastfive."W";
	}
	else{
		$lastfive = $lastfive."L";
	}
}
$distribution = 0;
$sql_dis = "select sum(distribution) as dist_amt from tippurchases where tip_id in (select id from membertips where member_id = $user_id)";
$res_dis = mysqli_query($con,$sql_dis);
$result_dis = 0;
while($dis = mysqli_fetch_assoc($res_dis)){
	$result_dis = $dis['dist_amt'];
}
if($result_dis == "" || $result_dis == null){
	$result_dis = 0; 
}
$results['result_details']['distribution'] = $result_dis;
$results['result_details']['total_wins'] = $wins;
$results['result_details']['total_loss'] = $loss; 
$results['result_details']['last_five'] = $lastfive;
$results['result_details']['profit'] = $profit;
$results['result_details']['roi'] = $roi;


// FOR NEW GRAPH STARTS 

$slf = "select added,running_profit,running_stake FROM tipscore WHERE member_id = $user_id AND is_void = '0' ORDER BY running_stake ASC";
$ref = mysqli_query($con,$slf);
$tips_graph = array();
while($pf = mysqli_fetch_assoc($ref)){
	$tips_graph[] = $pf;
}

$results['result_details']['graph'] = $tips_graph;

// FOR NEW GRAPH ENDS 

echo json_encode(array('status'=>1,'data'=>$results));




/**************** FUNCTIONS SECTION STARTS ******************/

function get_like_dislike($id,$con,$status){
     $sql_get_user_detail = "select count(id) as total from `tiplikes` where tip_id = $id and liketype = '$status'";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['total'];		  
               }
            return "0";
}

function get_comment_count($id,$con){
     $sql_get_user_detail = "select 	count(id) as total from `tipcomments` where tip_id = $id";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['total'];		  
               }
            return "0";

}


function is_past($event_date){

$utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",time()),
			    new DateTimeZone('UTC')
		);

		$mytime = clone $utc_date;
	 $mytime->setTimeZone(new DateTimeZone(date_default_timezone_get()));
	 $utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",strtotime($event_date)),
			    new DateTimeZone('UTC')
			);

			$acst_date = clone $utc_date;
			$acst_date->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			$past = ($acst_date<$mytime)?1:0;
          
		return $past;
}

function get_user_betting_result($con,$uid,$status){

		$count = 0;
		if($status == "win"){
		    $sql = "select count(*) as total_count from tipscore where member_id = $uid AND profit > 0 AND is_void = '0'";
			$res_sql = mysqli_query($con,$sql);
			$post = mysqli_fetch_assoc($res_sql);
			$count = $post['total_count'];
		}
		if($status == "loss"){
		    $sql = "select count(*) as total_count from tipscore where member_id = $uid AND profit <= 0 AND is_void = '0'";
			$res_sql = mysqli_query($con,$sql);
			$post = mysqli_fetch_assoc($res_sql);
			$count = $post['total_count'];
		}
		
	
	return $count;
}	

function get_user_profit($con,$uid){
    $sql_get_user_detail = "select running_profit from tipscore where member_id = $uid order by id DESC limit 1";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['running_profit'];		  
               }
       return "0";
}

function get_user_running_stake($con,$user_id){
	$sql_get_user_detail = "select running_stake from tipscore where member_id = $user_id order by id DESC limit 1";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['running_stake'];		  
               }
       return "0";
}

function roipercent($user,$con) {
	
	$id = $user;
    $roi = 0;
	$sql = "select * FROM tipscore where member_id = $id AND is_void = '0' ORDER BY id DESC";
	$res = mysqli_query($con,$sql);
	$counter = mysqli_num_rows($res);
	if($counter <= 0){
		$roi = 0;
	}
	else{
	    $post = mysqli_fetch_assoc($res);
		$running_profit = $post['running_profit'];
		$running_stake = $post['running_stake'];
		$roi = ($running_profit/$running_stake)*100;
	}	
	return number_format($roi,2).'%';
}


?>
