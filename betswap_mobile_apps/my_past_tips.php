<?php 

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include("connection.php");

$user_id = isset($_POST['uid']) ? $_POST['uid'] : 343;
$page_no = isset($_POST['page_no']) ? $_POST['page_no'] : 1;

$past_tips = get_tips($con,$user_id,"past",$page_no);

$results['bet_history'] = $past_tips;

if(count($results)>0){
	echo json_encode(array('status'=>1,'data'=>$results));
}else{
	echo json_encode(array('status'=>0,'data'=>array()));
}


function get_tips($con,$user_id,$status,$page_no){
$results = array();
$page = ($page_no - 1) * 20;
if($status == "past"){
 	$sql_present_tip = "SELECT * FROM membertips where member_id = $user_id and expires < NOW() order by expires DESC limit $page , 20";
}
else{
	$sql_present_tip = "SELECT * FROM membertips where member_id = $user_id and expires >= NOW() order by expires ASC limit $page , 20";
}	
$res_present_tip = mysqli_query($con,$sql_present_tip);

$data1 = get_data($res_present_tip,$user_id,$con);

$results = $data1;
return $results;
}

function get_data($res,$user_id,$con){
$results = array();
$z = 0;
while($po = mysqli_fetch_assoc($res)){
		   $id = $po['id'];
     	   $bettype = $po['bettype'];
     	   $multistake = $po['multistake'];
     	   $price = $po['price'];  
		   $purchased_status = "yes";
     	   $sql_ti = "select * from tipitems where tip_id = '$id'";
     	   $res_ti = mysqli_query($con,$sql_ti);
           $like_count = get_like_dislike($id,$con,1);
		   $dislike_count = get_like_dislike($id,$con,0);
		   $comment_count = get_comment_count($id,$con);
		   $results[$z]['id'] = $id;
		   $results[$z]['bettype'] = $bettype;
		   $results[$z]['multistake'] = $multistake;
		   $results[$z]['price'] = $price;
		   $results[$z]['purchased_status'] = $purchased_status;
		   $results[$z]['like_count'] = $like_count;
		   $results[$z]['dislike_count'] = $dislike_count;
		   $results[$z]['comment_count'] = $comment_count;
		   $i = 0;
     	   while($p = mysqli_fetch_assoc($res_ti)){
			$market_id = $p['market_id']; 
			$sql_market_details = "select * from markets where id = $market_id";
			$res_market_details = mysqli_query($con,$sql_market_details);
			$po_market_details = mysqli_fetch_assoc($res_market_details);
			$market_cat_id = $po_market_details['cat_id'];
			$sql_market_cat_name = "select * from marketcategories where id = $market_cat_id";
			$res_market_cat_name = mysqli_query($con,$sql_market_cat_name);
			$po_market_cat_name = mysqli_fetch_assoc($res_market_cat_name);
     	   	$results[$z]['tips_item'][$i]['market_id'] = $p['market_id']; 
			$results[$z]['tips_item'][$i]['market_name'] = $po_market_details['name'];
			$results[$z]['tips_item'][$i]['market_cat_name'] = $po_market_cat_name['name'];
     	   	$results[$z]['tips_item'][$i]['event_id'] = $p['event_id']; 
     	   	$results[$z]['tips_item'][$i]['cat_id'] = $p['cat_id'];
     	   	$results[$z]['tips_item'][$i]['odds'] = $p['odds']; 
     	   	$results[$z]['tips_item'][$i]['stake'] = $p['stake'];
			$is_voide = $p['is_void'];	
			$is_marked = $p['is_marked'];	
			$check_required = $p['check_required'];	
			$result_event = $p['result'];	
			
			$cat_id = $p['cat_id'];
				$cat_name = get_cat_name_by_id($cat_id,$con);
				$results[$z]['tips_item'][$i]['cat_name'] = $cat_name; 
               $eid = $p['event_id'];
               
                $sql_get_user_detail = "SELECT * FROM events where id = $eid";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  $results[$z]['tips_item'][$i]['event_name'] = $r['name'];
                  $results[$z]['tips_item'][$i]['event_date'] = $r['date'];
				  
                  $results[$z]['tips_item'][$i]['event_venue'] = $r['venue'];
				  $past = is_past($r['date']);			  
               }
			
			$flag = 0;
            if($past == 1){
			    if($check_required == 1){
				    $flag = 'Verification Needed';
				} 
				elseif($is_marked == 0){
				    $flag = 'Result: Pending';
				}
				elseif($result_event == 1){
					$flag = "Result: WIN";
				}
				elseif($is_voide == 0){
				    $flag = "Result: LOSS";
				}
				else{
				    $flag = "Result: VOID"; 
				}
			}           
            $results[$z]['tips_item'][$i]['result'] = $flag;   
     	   	$i++; 
     	   }
		   
		   $uid = $user_id;
           
          $sql_get_user_detail = "SELECT * FROM members where id = $uid";
          $res_user = mysqli_query($con,$sql_get_user_detail);
		  $user_arr=[];
         while($r = mysqli_fetch_assoc($res_user)){
			 $user_arr['member_id'] = $r['id'];
             $user_arr['member_email'] = $r['email'];
             $user_arr['member_image'] = $r['profile_url'];
             $user_arr['firstname'] = $r['firstname'];
             $user_arr['lastname'] = $r['lastname'];
			 $wins = get_user_betting_result($con,$uid,"win");
			 $loss = get_user_betting_result($con,$uid,"loss");
			 $profit = get_user_profit($con,$uid);
			 $user_arr['wins'] = $wins;
			 $user_arr['loss'] = $loss;
			 $user_arr['profit'] = $profit;
         }
         $results[$z]['user_detail'][0]=$user_arr;
	$z++;	   
}
return $results;

}

function get_like_dislike($id,$con,$status){
     $sql_get_user_detail = "select count(id) as total from `tiplikes` where tip_id = $id and liketype = '$status'";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['total'];		  
               }
            return "0";
}

function get_comment_count($id,$con){
     $sql_get_user_detail = "select 	count(id) as total from `tipcomments` where tip_id = $id";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['total'];		  
               }
            return "0";

}

function is_past($event_date){

$utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",time()),
			    new DateTimeZone('UTC')
		);

		$mytime = clone $utc_date;
	 $mytime->setTimeZone(new DateTimeZone(date_default_timezone_get()));
	 $utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",strtotime($event_date)),
			    new DateTimeZone('UTC')
			);

			$acst_date = clone $utc_date;
			$acst_date->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			$past = ($acst_date<$mytime)?1:0;
          
		return $past;
}
function get_cat_name_by_id($cat_id,$con){

                   $sql_get_user_detail = "select name from `categories` where id = $cat_id ";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['name'];		  
               }
            return "-";
}

function get_user_betting_result($con,$uid,$status){

		$count = 0;
		if($status == "win"){
		    $sql = "select count(*) as total_count from tipscore where member_id = $uid AND profit > 0 AND is_void = '0'";
			$res_sql = mysqli_query($con,$sql);
			$post = mysqli_fetch_assoc($res_sql);
			$count = $post['total_count'];
		}
		if($status == "loss"){
		    $sql = "select count(*) as total_count from tipscore where member_id = $uid AND profit <= 0 AND is_void = '0'";
			$res_sql = mysqli_query($con,$sql);
			$post = mysqli_fetch_assoc($res_sql);
			$count = $post['total_count'];
		}
		
	
	return $count;
}	

function get_user_profit($con,$uid){
    $sql_get_user_detail = "select running_profit from tipscore where member_id = $uid order by id DESC limit 1";
               $res_user = mysqli_query($con,$sql_get_user_detail);
               while($r = mysqli_fetch_assoc($res_user)){
                  return $r['running_profit'];		  
               }
       return "0";
}
?>