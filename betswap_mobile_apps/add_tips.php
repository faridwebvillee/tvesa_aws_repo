<?php
header("Access-Control-Allow-Origin: *");
include("connection.php");

$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : 0;
$is_multi = isset($_POST['ismulti']) ? $_POST['ismulti'] : "";
$price = isset($_POST['price']) ? $_POST['price'] : 0;
$data = isset($_POST['data']) ? $_POST['data'] : 0;

if($user_id == 0 || $is_multi == "" || $data == 0){
    header('Content-Type: application/json');
    echo json_encode(array("message"=>'fail')); 
}
else{
     if($is_multi == 1){
          $now = date("Y-m-d H:i:s");
          $arr = array();
		  for($z=0;$z<count($data);$z++){
			$arr[] = $data[$z]['event_date'];
		  }
          $heighest_date = get_max_date($arr);
		  $stakes = $data[0]['stakes'];
          $sql_insert1 = "INSERT INTO `membertips` (`member_id`,`bettype`,`multistake`,`price`,`added`,`expires`) VALUES ('$user_id','multi','$stakes','$price','$now','$heighest_date')";      
          $res = mysqli_query($con,$sql_insert1);
          $insert_id = mysqli_insert_id($con);
          for($i=0;$i<count($data);$i++){
				 $market_ids = $data[$i]['market_id'];
				 $oddss = $data[$i]['odds'];
				 $event_cats = $data[$i]['cate_id'];
				 $events_ids = $data[$i]['event_id'];
				 $sql_tips_item = "INSERT INTO `tipitems` (`tip_id`,`member_id`,`market_id`,`stake`,`odds`,`line`,".
				 "`cat_id`,`event_id`,`is_void`,`is_marked`,`check_required`,`result`) VALUES ".
				  "('$insert_id','$user_id','$market_ids','1','$oddss','','$event_cats','$events_ids','0','0','0','0')";	
				  mysqli_query($con,$sql_tips_item);
          }
          header('Content-Type: application/json');
    		 echo json_encode(array("message"=>'success')); 
     }
     else{
          $now = date("Y-m-d H:i:s");
          $arr = array();
		  for($z=0;$z<count($data);$z++){
			$arr[] = $data[$z]['event_date'];
		  }
          $heighest_date = get_max_date($arr);
          $sql_insert1 = "INSERT INTO `membertips` (`member_id`,`bettype`,`multistake`,`price`,`added`,`expires`) VALUES ('$user_id','single','0','$price','$now','$heighest_date')";
          $res = mysqli_query($con,$sql_insert1);
          $insert_id = mysqli_insert_id($con);
          for($i=0;$i<count($data);$i++){
				 $market_ids = $data[$i]['market_id'];
				 $oddss = $data[$i]['odds'];
				 $event_cats = $data[$i]['cate_id'];
				 $events_ids = $data[$i]['event_id'];
				 $stakes_vals = $data[$i]['stakes'];
				 $sql_tips_item = "INSERT INTO `tipitems` (`tip_id`,`member_id`,`market_id`,`stake`,`odds`,`line`,".
				 "`cat_id`,`event_id`,`is_void`,`is_marked`,`check_required`,`result`) VALUES ".
				  "('$insert_id','$user_id','$market_ids','$stakes_vals','$oddss','','$event_cats','$events_ids','0','0','0','0')";	
				  mysqli_query($con,$sql_tips_item);
          }
          header('Content-Type: application/json');
    		 echo json_encode(array("message"=>'success'));
     }
}



function get_max_date($dates){
$mostRecent= 0;
$pos = 0;
foreach($dates as $key => $date){
  $curDate = strtotime($date);
  if ($curDate > $mostRecent) {
     $mostRecent = $curDate;
     $pos = $key;
  }
}
return $dates[$pos];
}
?>