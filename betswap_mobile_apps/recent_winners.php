<?php 

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
include("connection.php");

$sql_win_tip_ids = "SELECT tip_id FROM tipscore WHERE profit > 0 ORDER BY added DESC LIMIT 10";
$res_win_tip_ids = mysqli_query($con,$sql_win_tip_ids);
$tip_ids = 0;
$i=0;
$result = array();
while($post_win_tip_ids = mysqli_fetch_assoc($res_win_tip_ids)){
	$tip_ids = $tip_ids.",".$post_win_tip_ids['tip_id']; 
} 
$sql_tips = "select * from tipitems where id in ($tip_ids)";
$res_tips = mysqli_query($con,$sql_tips);
while($post = mysqli_fetch_assoc($res_tips)){
	$tip_id = $post['tip_id'];
	$member_id = $post['member_id'];
	$event_id = $post['event_id'];
	$car_id = $post['cat_id'];
	$market_id = $post['market_id'];
	$stake = $post['stake'];
	$odds = $post['odds'];
	$line = $post['line'];
	
	$sql_event = "select * from events where id = $event_id";
	$res_event = mysqli_query($con,$sql_event);
	$post_event = mysqli_fetch_assoc($res_event);
	$event_name = $post_event['name'];
	$event_date = $post_event['date'];
	
	$sql_cat = "select * from categories where id = $car_id";
	$res_cat = mysqli_query($con,$sql_cat);
	$post_cat = mysqli_fetch_assoc($res_cat);
	$cat_name = $post_cat['name'];
	
	$sql_market = "select * from markets where id = $market_id";
	$res_market = mysqli_query($con,$sql_market);
	$post_market = mysqli_fetch_assoc($res_market);
	$market_name = $post_market['name'];
	$market_cat_id = $post_market['cat_id'];
	
	$sql_market_cat = "select * from marketcategories where id = $market_cat_id";
	$res_market_cat = mysqli_query($con,$sql_market_cat);
	$post_market_cat = mysqli_fetch_assoc($res_market_cat);
	$market_cat_name = $post_market_cat['name'];
	
	$return = $stake * $odds;
	$profit = $return - $stake;
	
	$sql_user = "select * from members where id = $member_id";
	$res_user = mysqli_query($con,$sql_user);
	$post_user = mysqli_fetch_assoc($res_user);
	$name = $post_user['username'];
	$profile_url = $post_user['profile_url'];
	$total_wins = get_user_betting_result($con,$member_id,"win");
	$total_loss = get_user_betting_result($con,$member_id,"loss");
	
	$user_array = array("id"=>$member_id,"name"=>$name,"profile_url"=>$profile_url,"total_wins"=>$total_wins,"total_loss"=>$total_loss);
	
	$tip_details = array("cat_name"=>$cat_name,"event_name"=>$event_name,"event_date"=>$event_date,"market_cat_name"=>$market_cat_name,"market_name"=>$market_name,"stake"=>$stake,"odds"=>$odds,"line"=>$line,"profit"=>$profit);
	
	$result[$i]['tip_id'] = $tip_id;
	$result[$i]['tip_details'] = $tip_details;
	$result[$i]['user_details'] = $user_array;
	
	$i++;
	
}

echo json_encode(array('status'=>1,'data'=>$result));

function get_user_betting_result($con,$uid,$status){

		$count = 0;
		if($status == "win"){
		    $sql = "select count(*) as total_count from tipscore where member_id = $uid AND profit > 0 AND is_void = '0'";
			$res_sql = mysqli_query($con,$sql);
			$post = mysqli_fetch_assoc($res_sql);
			$count = $post['total_count'];
		}
		if($status == "loss"){
		    $sql = "select count(*) as total_count from tipscore where member_id = $uid AND profit <= 0 AND is_void = '0'";
			$res_sql = mysqli_query($con,$sql);
			$post = mysqli_fetch_assoc($res_sql);
			$count = $post['total_count'];
		}
		
	
	return $count;
}	

function is_past($event_date){

$utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",time()),
			    new DateTimeZone('UTC')
		);

		$mytime = clone $utc_date;
	 $mytime->setTimeZone(new DateTimeZone(date_default_timezone_get()));
	 $utc_date = DateTime::createFromFormat(
			    'Y-m-d G:i',
			    date("Y-m-d G:i",strtotime($event_date)),
			    new DateTimeZone('UTC')
			);

			$acst_date = clone $utc_date;
			$acst_date->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			$past = ($acst_date<$mytime)?1:0;
          
		return $past;
}
?>